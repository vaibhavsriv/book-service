INSERT into AUTHOR_ENTITY values(10001,'Bob');
INSERT into AUTHOR_ENTITY values(10002,'Martin');

INSERT into CATEGORY_ENTITY values(20001,'Computer');
INSERT into CATEGORY_ENTITY values(20002,'Finance');
INSERT into CATEGORY_ENTITY values(20003,'Journal');
INSERT into CATEGORY_ENTITY values(20004,'Marketing');
INSERT into CATEGORY_ENTITY values(20005,'Electronics');


INSERT into BOOK_ENTITY values(30001,'Computer Programming','Let Us C');
INSERT into BOOK_ENTITY values(30002,'Computer Programming','Let Us C++');
INSERT into BOOK_ENTITY values(30003,'Marketing','Let Us Marketing');
INSERT into BOOK_ENTITY values(30004,'Finance','Let Us Finance');
INSERT into BOOK_ENTITY values(30005,'Electronics','Let Us Electronics');



INSERT into BOOK_CATEGORY values(30001,20001);
INSERT into BOOK_CATEGORY values(30002,20002);
INSERT into BOOK_CATEGORY values(30003,20003);
INSERT into BOOK_CATEGORY values(30004,20004);
INSERT into BOOK_CATEGORY values(30005,20005);


INSERT into BOOK_AUTHOR values(30001,10001);
INSERT into BOOK_AUTHOR values(30002,10002);
INSERT into BOOK_AUTHOR values(30003,10001);
INSERT into BOOK_AUTHOR values(30004,10002);
INSERT into BOOK_AUTHOR values(30005,10001);