package com.jpop.bookservice.converter;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.jpop.bookservice.dto.AuthorDTO;
import com.jpop.bookservice.dto.BookDTO;
import com.jpop.bookservice.dto.CategoryDTO;
import com.jpop.bookservice.entity.AuthorEntity;
import com.jpop.bookservice.entity.BookEntity;
import com.jpop.bookservice.entity.CategoryEntity;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ModelToEntityConverter {

	public static BookEntity toEntity(BookDTO dto) {
		Set<CategoryEntity> categories = null;
		if (null != dto.getCategories()) {
			categories = dto.getCategories().parallelStream().map(category -> {
				CategoryEntity categoryEntity = new CategoryEntity();
				categoryEntity.setName(category.getName());
				return categoryEntity;
			}).collect(Collectors.toSet());

		}

		Set<AuthorEntity> authors = null;
		if (null != dto.getAuthors()) {
			authors = dto.getAuthors().parallelStream().map(author -> {
				AuthorEntity bookAuthor = new AuthorEntity();
				bookAuthor.setName(author.getName());
				return bookAuthor;
			}).collect(Collectors.toSet());
		}

		BookEntity bookEntity = new BookEntity();
		bookEntity.setName(dto.getName());
		bookEntity.setDesc(dto.getDesc());
		bookEntity.setAuthors(authors);
		bookEntity.setCategory(categories);
		return bookEntity;
	}

	public static BookEntity toExistingEntity(BookEntity bookEntity, BookDTO bookDTO) {
		Set<CategoryEntity> categories = null;
		if (null != bookDTO.getCategories()) {
			categories = bookDTO.getCategories().parallelStream().map(category -> {
				CategoryEntity bookCategory = new CategoryEntity();
				bookCategory.setName(category.getName());
				return bookCategory;
			}).collect(Collectors.toSet());

		}

		Set<AuthorEntity> authors = null;
		if (null != bookDTO.getAuthors()) {
			authors = bookDTO.getAuthors().parallelStream().map(author -> {
				AuthorEntity bookAuthor = new AuthorEntity();
				bookAuthor.setName(author.getName());
				return bookAuthor;
			}).collect(Collectors.toSet());
		}

		bookEntity.setId(bookDTO.getId());
		bookEntity.setName(bookDTO.getName());
		bookEntity.setDesc(bookDTO.getDesc());
		bookEntity.setAuthors(authors);
		bookEntity.setCategory(categories);
		return bookEntity;
	}

	public static BookDTO toDTO(BookEntity entity) {
		List<AuthorDTO> bookAuthors = null;
		if (null != entity.getAuthors()) {
			bookAuthors = entity.getAuthors().parallelStream().map(author -> {
				AuthorDTO bookAuthor = new AuthorDTO();
				bookAuthor.setId(author.getId());
				bookAuthor.setName(author.getName());
				return bookAuthor;
			}).collect(Collectors.toList());
		}

		List<CategoryDTO> bookCategories = null;
		if (null != entity.getCategory()) {
			bookCategories = entity.getCategory().parallelStream().map(category -> {
				CategoryDTO bookCategory = new CategoryDTO();
				bookCategory.setId(category.getId());
				bookCategory.setName(category.getName());
				return bookCategory;
			}).collect(Collectors.toList());
		}

		BookDTO book = new BookDTO();
		book.setId(entity.getId());
		book.setName(entity.getName());
		book.setDesc(entity.getDesc());
		book.setAuthors(bookAuthors);
		book.setCategories(bookCategories);
		return book;
	}
}
