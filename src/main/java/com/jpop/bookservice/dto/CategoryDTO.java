package com.jpop.bookservice.dto;

import lombok.Data;

@Data
public class CategoryDTO {
	private long id;

	private String name;

}
