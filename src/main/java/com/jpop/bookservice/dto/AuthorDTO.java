package com.jpop.bookservice.dto;

import lombok.Data;

@Data
public class AuthorDTO {

	private long id;

	private String name;

}
