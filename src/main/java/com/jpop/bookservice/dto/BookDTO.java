package com.jpop.bookservice.dto;

import java.util.List;

import lombok.Data;

@Data
public class BookDTO {

	private long id;	
	private String name;
	private String desc;
	private List<AuthorDTO> authors;
	private List<CategoryDTO> categories;

	
	
}
