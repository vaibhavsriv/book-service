package com.jpop.bookservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.jpop.bookservice.entity.BookEntity;

@Repository
public interface BookRepository extends PagingAndSortingRepository<BookEntity, Long> {

}
