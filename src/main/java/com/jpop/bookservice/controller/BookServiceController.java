package com.jpop.bookservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.bookservice.dto.BookDTO;
import com.jpop.bookservice.service.BookServiceDelegate;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/v1")
public class BookServiceController {
	@Autowired
	private BookServiceDelegate bookServiceDelegate;

	@GetMapping("/books")
	public List<BookDTO> getAllBooks() {
		return bookServiceDelegate.getAllBooks();
	}

	@PostMapping("/books")
	@ApiOperation(value="Save the book")
	public BookDTO saveBook(@RequestBody BookDTO newBook) {
		return bookServiceDelegate.save(newBook);
	}

	@GetMapping("/books/{id}")
	public BookDTO getBookDetails(@PathVariable Long id) {
		return bookServiceDelegate.getBookDetails(id);

	}

	@PutMapping("/books/{id}")
	public BookDTO updateBook(@RequestBody BookDTO newBook, @PathVariable Long id) {

		return bookServiceDelegate.update(newBook, id);
	}

	@DeleteMapping("/books/{id}")
	public void deleteBook(@PathVariable Long id) {
		bookServiceDelegate.deleteBook(id);
	}

	public BookServiceDelegate getBookServiceDelegate() {
		return bookServiceDelegate;
	}

}
