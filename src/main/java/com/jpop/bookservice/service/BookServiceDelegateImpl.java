package com.jpop.bookservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpop.bookservice.converter.ModelToEntityConverter;
import com.jpop.bookservice.dto.BookDTO;
import com.jpop.bookservice.entity.BookEntity;
import com.jpop.bookservice.exception.BookNotFoundException;
import com.jpop.bookservice.repository.BookRepository;

@Service
public class BookServiceDelegateImpl implements BookServiceDelegate {

	@Autowired
	BookRepository repository;

	@Override
	public BookDTO save(BookDTO bookDto) {
		BookEntity entityToSave=ModelToEntityConverter.toEntity(bookDto);
		BookEntity savedEntity=repository.save(entityToSave);
		return ModelToEntityConverter.toDTO(savedEntity);
	}

	@Override
	public BookDTO update(BookDTO bookDto, Long id) {
		BookEntity newBook = ModelToEntityConverter.toEntity(bookDto);
		return repository.findById(id).map(x -> {
			x.setName(newBook.getName());
			x.setAuthors(newBook.getAuthors());
			x.setDesc(newBook.getDesc());
			x.setCategory(newBook.getCategory());
			BookEntity entity = repository.save(x);
			return ModelToEntityConverter.toDTO(entity);
		}).orElseGet(() -> {
			newBook.setId(id);
			BookEntity entity = repository.save(newBook);
			return ModelToEntityConverter.toDTO(entity);

		});
	}

	@Override
	public void deleteBook(Long id) {
		repository.deleteById(id);

	}

	@Override
	public BookDTO getBookDetails(Long id) {
		return repository.findById(id).map(entity ->  ModelToEntityConverter.toDTO(entity))
				.orElseThrow(() -> new BookNotFoundException(id));
	}

	@Override
	public List<BookDTO> getAllBooks() {
		List<BookDTO> bookList = new ArrayList<>();
		repository.findAll().forEach(book -> bookList.add(ModelToEntityConverter.toDTO(book)));
		return bookList;
	}

	
	

}
