package com.jpop.bookservice.service;

import java.util.List;

import com.jpop.bookservice.dto.BookDTO;

public interface BookServiceDelegate {

	public BookDTO save(BookDTO bookDto);

	public BookDTO update(BookDTO bookDto, Long id);

	public void deleteBook(Long id);

	public BookDTO getBookDetails(Long id);

	public List<BookDTO> getAllBooks();

}
