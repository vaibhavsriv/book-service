 package com.jpop.bookservice;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jpop.bookservice.entity.AuthorEntity;
import com.jpop.bookservice.entity.BookEntity;
import com.jpop.bookservice.entity.CategoryEntity;
import com.jpop.bookservice.repository.BookRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BookServiceApplicationTests {

	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BookRepository mockRepository;

	@Before
	public void init() {

		BookEntity book = generateTestData("Let Us Test", "Testing");

		when(mockRepository.findById(1L)).thenReturn(Optional.of(book));

	}

	@Test
	public void find_bookId_OK() throws Exception {

		mockMvc.perform(get("/books/1"))
				/* .andDo(print()) */
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.name", is("Let Us Test")))
				.andExpect(jsonPath("$.desc", is("Testing")));

		verify(mockRepository, times(1)).findById(1L);

	}

	@Test
	public void find_bookIdNotFound_404() throws Exception {
		mockMvc.perform(get("/books/5")).andExpect(status().isNotFound());
	}

	@Test
	public void delete_book_OK() throws Exception {

		doNothing().when(mockRepository).deleteById(1L);

		mockMvc.perform(delete("/books/1"))
				/* .andDo(print()) */
				.andExpect(status().isOk());

		verify(mockRepository, times(1)).deleteById(1L);
	}

	@Test
	public void testSaveBook() throws Exception {

		BookEntity newBook = generateTestData("Spring Boot Guide", "Java");
		when(mockRepository.save(any(BookEntity.class))).thenReturn(newBook);

		mockMvc.perform(post("/books").content(om.writeValueAsString(newBook)).header(HttpHeaders.CONTENT_TYPE,
				MediaType.APPLICATION_JSON))
				/* .andDo(print()) */
				.andExpect(status().isCreated()).andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.name", is("Spring Boot Guide"))).andExpect(jsonPath("$.desc", is("Java")));

		verify(mockRepository, times(1)).save(any(BookEntity.class));

	}

	@Test
	public void update_book_OK() throws Exception {

		BookEntity updateBook = generateTestData("Updated Test", "Update Desc");
		when(mockRepository.save(any(BookEntity.class))).thenReturn(updateBook);

		mockMvc.perform(put("/books/1").content(om.writeValueAsString(updateBook)).header(HttpHeaders.CONTENT_TYPE,
				MediaType.APPLICATION_JSON)).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.name", is("Updated Test"))).andExpect(jsonPath("$.desc", is("Update Desc")));

	}

	private BookEntity generateTestData(String name, String desc) {
		CategoryEntity category = new CategoryEntity();
		category.setId(1001);
		category.setName("Test Category");

		AuthorEntity author = new AuthorEntity();
		author.setId(2001);
		author.setName("Test Author");
		Set<CategoryEntity> categorySet = new HashSet<CategoryEntity>();
		categorySet.add(category);
		Set<AuthorEntity> authorSet = new HashSet<AuthorEntity>();
		authorSet.add(author);
		BookEntity book = new BookEntity();
		book.setId(1L);
		book.setName(name);
		book.setDesc(desc);
		book.setCategory(categorySet);
		book.setAuthors(authorSet);
		return book;
	}

}
