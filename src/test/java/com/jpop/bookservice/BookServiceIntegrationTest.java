package com.jpop.bookservice;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jpop.bookservice.controller.BookServiceController;
import com.jpop.bookservice.dto.AuthorDTO;
import com.jpop.bookservice.dto.BookDTO;
import com.jpop.bookservice.dto.CategoryDTO;
import com.jpop.bookservice.exception.BookNotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BookServiceIntegrationTest {

	@Autowired
	BookServiceController bookServiceController;

	@Test
	public void testCreateReadDelete() {

		BookDTO book = generateTestData("Let Us Test", "Testing");
		BookDTO bookDto = bookServiceController.saveBook(book);

		BookDTO bookData = bookServiceController.getBookDetails(bookDto.getId());
		Assertions.assertThat(bookData).hasFieldOrPropertyWithValue("name", "Let Us Test");

		bookServiceController.deleteBook(bookDto.getId());
		Assertions.assertThatExceptionOfType(BookNotFoundException.class)
				.isThrownBy(() -> bookServiceController.getBookDetails(bookDto.getId()));
	}

	private BookDTO generateTestData(String name, String desc) {
		CategoryDTO category = new CategoryDTO();
		category.setId(1001);
		category.setName("Test Category");

		AuthorDTO author = new AuthorDTO();
		author.setId(2001);
		author.setName("Test Author");
		List<CategoryDTO> categorySet = new ArrayList<>();
		categorySet.add(category);
		List<AuthorDTO> authorSet = new ArrayList<>();
		authorSet.add(author);
		BookDTO book = new BookDTO();
		book.setId(10009);
		book.setName(name);
		book.setDesc(desc);
		book.setCategories(categorySet);
		book.setAuthors(authorSet);
		return book;
	}

}
