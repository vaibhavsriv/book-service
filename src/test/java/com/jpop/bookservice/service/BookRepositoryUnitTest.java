package com.jpop.bookservice.service;

import java.util.HashSet;
import java.util.Set;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.jpop.bookservice.entity.AuthorEntity;
import com.jpop.bookservice.entity.BookEntity;
import com.jpop.bookservice.entity.CategoryEntity;
import com.jpop.bookservice.repository.BookRepository;

@RunWith(SpringRunner.class)

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class BookRepositoryUnitTest {
	
	@Autowired
	BookRepository bookRepository;
	
	@Test
	  public void testCreateReadDelete() {
	 BookEntity bookEntity = generateTestData("Let Us Test", "Testing");

	 bookRepository.save(bookEntity);

	    Iterable<BookEntity> bookList = bookRepository.findAll();
	    Assertions.assertThat(bookList).extracting(BookEntity::getName).contains("Let Us Test");

	    bookRepository.deleteAll();
	    Assertions.assertThat(bookRepository.findAll()).isEmpty();
	    }
	
	private BookEntity generateTestData(String name, String desc) {
		CategoryEntity category = new CategoryEntity();
		category.setId(1001);
		category.setName("Test Category");

		AuthorEntity author = new AuthorEntity();
		author.setId(2001);
		author.setName("Test Author");
		Set<CategoryEntity> categorySet = new HashSet<CategoryEntity>();
		categorySet.add(category);
		Set<AuthorEntity> authorSet = new HashSet<AuthorEntity>();
		authorSet.add(author);
		BookEntity book = new BookEntity();
		book.setId(1L);
		book.setName(name);
		book.setDesc(desc);
		book.setCategory(categorySet);
		book.setAuthors(authorSet);
		return book;
	}

}
